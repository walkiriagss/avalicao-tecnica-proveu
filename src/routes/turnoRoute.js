const express = require('express');
const router = express.Router();
const turnoController = require('../controllers/turnoController')

router.post('/turno', turnoController.turno) ;
module.exports = router;
