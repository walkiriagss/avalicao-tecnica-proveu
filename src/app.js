const express = require('express')
const bodyParser = require('body-parser')
const app = express();
app.use(bodyParser.json())
const router = express.Router();
//Rotas da API
const index = require('./routes/index');
const turnoRoute = require('./routes/turnoRoute');

app.use(index);
app.use(turnoRoute);

module.exports = app;

