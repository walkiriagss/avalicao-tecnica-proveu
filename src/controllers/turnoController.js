
//método responsável por calculo de horas no turno noturno e no turno diurno
var moment = require('moment'); // require

exports.turno = (req, res) => {
    var entrada = moment("23:59", "HH:mm");
    var saida = moment("08:02", "HH:mm"); 
    var horasDiurno = moment("00:00", "HH:mm");
    var horasNoturno = moment("00:00", "HH:mm");
    var diurmo = moment('05:00', "HH:mm");
    var norutno = moment('22:00', "HH:mm");
    const meiaNoite = moment('24:00', "HH:mm");
    var cont = moment(entrada)

    entrada = moment(entrada, "HH:mm")
    saida = moment(saida, "HH:mm")

        if(moment(entrada).isAfter(moment(saida))){
            saida = moment(saida).add(1,'day')
        }
        cont = moment(cont).add(1,'minute')
        if(moment(cont).isSame(moment(meiaNoite))){
            norutno = moment(norutno).add(1,'day')
            diurmo = moment(diurmo).add(1, 'day')
        }
        while((moment(cont).isBetween(moment(entrada), moment(saida)))){  
            if(moment(cont).isAfter(moment(diurmo))){
                if(moment(cont).isBefore(moment(norutno))){
                    cont = moment(cont).add(1,'minute')
                    horasDiurno = moment(horasDiurno).add(1,'minute')  
                }
                else{
                    cont = moment(cont).add(1,'minute')
                    horasNoturno = moment(horasNoturno).add(1,'minute')
                    
                    if(moment(cont).isSame(moment(meiaNoite))){
                        norutno = moment(norutno).add(1,'day')
                        diurmo = moment(diurmo).add(1, 'day')
                    }
                }  
            }
            else{
                cont = moment(cont).add(1,'minute')
                horasNoturno = moment(horasNoturno).add(1,'minute')     
            }
        }
           console.log("noturno:", moment(horasNoturno).format('h:mm'))
           console.log("diurno:", moment(horasDiurno).format('h:mm'))
            return(res.status(201).send({
            horasDiurno, 
            horasNoturno,
        }))
};